""" Project Euler 2

Jeder neue Term in der Fibonacci-Reihe wird gebildet, 
indem die beiden vorherigen Zahlen addiert werden.
Wenn man mit 1 und 2 beginnt, sind die ersten 10 Terme wie folgt:
1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...

Finden Sie die Summe aller geraden Terme der Fibonacci-Reihe, 
die 4 Millionen nicht ueberschreiten.

"""


def fibonacci(n):
    if n == 1:
        return 1

    if n == 2:
        return 2

    return fibonacci(n - 2) + fibonacci(n - 1)


def main():
    summe = 0
    weiter = True
    x = 1

    while weiter:
        fibo = fibonacci(x)

        if fibo % 2 == 0:
            summe += fibo

        if fibo > 4000000:
            weiter = False

        x += 1

    print 'Die Summe betraegt:', summe

if __name__ == '__main__':
    main()
