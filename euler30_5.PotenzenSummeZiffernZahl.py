""" Project Euler 30

Ueberraschenderweise gibt es nur 3 Zahlen, die als Summe der vierten Potenzen
ihrer Ziffern geschrieben werden koennen:

1634 = 1^4 + 6^4 + 3^4 + 4^4
8208 = 8^4 + 2^4 + 0^4 + 8^4
9474 = 9^4 + 4^4 + 7^4 + 4^4
Da 1 = 14 keine Summe ist, ist es nicht enthalten.

Die Summe dieser Zahlen ist 1634 + 8208 + 9474 = 19316.

Finden Sie die Summe aller Zahlen, die als Summe der 5. Potenzen ihrer Ziffern geschrieben
werden koennen.

"""


def potenz_bilden(zahl):
    summe = 0

    for ziffer in zahl:
        summe += int(ziffer) ** 5

    if summe == int(zahl):
        return True

    return False

print sum([x for x in range(2, 1000000) if potenz_bilden(str(x)) == True])
