""" Euler Project 48

The series, 1^1 + 2^2 + 3^3 + ... + 10^10 = 10405071317.
Find the last ten digits of the series, 1^1 + 2^2 + 3^3 + ... + 1000^1000.

"""

summe = 0

for i in range(1, 1001):
    potenz = i ** i
    summe += potenz

print str(summe)[-10:]

# print(str(sum([pow(i,i) for i in range(1,1001)]))[-10:])
# print sum(x ** x for x in range(1, 1001)) % (10 ** 10)
