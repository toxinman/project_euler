""" Project Euler 03

The prime factors of 13195 are 5, 7, 13 and 29.
What is the largest prime factor of the number 600851475143 ?

"""


def primzahl(zahl):
    prim = True

    if zahl == 1:
        prim = False
    else:
        i = 2
        while i < zahl:
            if zahl % i == 0:
                prim = False
            i += 1

    return prim


def teiler_ermitteln(zahl):
    return [x for x in range(1, 100001) if zahl % x == 0]


def main():
    teiler = teiler_ermitteln(600851475143.0)
    print max([x for x in teiler if primzahl(x)])

if __name__ == '__main__':
    main()
