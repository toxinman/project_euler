""" Project Euler 06

Die Summe der Quadrate der ersten 10 natuerlichen Zahlen ist
1^2 + 2^2 + ... + 10^2 = 385
Das Quadrat der Summe der ersten 10 natuerlichen Zahlen ist
(1 + 2 + ... + 10)2 = 55^2 = 3025

Somit ist die Differenz aus der Summe der Quadrate der ersten 10 natuerlichen Zahlen und dem Quadrat der Summe 3025 - 385 = 2640.
Finden Sie die Differenz aus der Summe der Quadrate der ersten 100 natuerlichen Zahlen und dem Quadrat der Summe.

"""

summe_der_quadrate = sum([x ** 2 for x in range(1, 101)])
quadrat_der_summe = sum([x for x in range(1, 101)]) ** 2

print quadrat_der_summe - summe_der_quadrate
