""" Project Euler 04

A palindromic number reads the same both ways. The largest
palindrome made from the product of two 2-digit numbers is
9009 = 91 * 99.

Find the largest palindrome made from the product of
two 3-digit numbers.

"""

hoechstes_produkt = 0

for faktor1 in range(1, 1000):
    for faktor2 in range(1, 1000):
        produkt = faktor1 * faktor2
        if produkt == int(str(produkt)[::-1]):
            if produkt > hoechstes_produkt:
                hoechstes_produkt = produkt

print hoechstes_produkt
