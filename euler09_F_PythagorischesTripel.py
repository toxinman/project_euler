""" Project Euler 09

Ein pythagoreisches Tripel ist eine Menge von 3 natuerlichen Zahlen, a < b < c, fuer die gilt:

a^2 + b^2 = c^2
Beispiel: 3^2 + 4^2 = 9 + 16 = 25 = 5^2.

Es existiert genau ein pythagoreisches Tripel, fuer das a + b + c = 1000 gilt.
Finden Sie das Produkt abc.

"""

for a in range(1, 1001):
    for b in range(1, 1001):
        for c in range(1, 1001):
            if a < b < c and a ** 2 + b ** 2 == c ** 2:
                if a + b + c == 1000:
                    print 'a:', a, 'b:', b, 'c:', c
                    print 'Produkt:', a * b * c
