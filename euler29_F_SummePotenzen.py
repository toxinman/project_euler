""" Project Euler 29

Wir betrachten alle ganzzahligen Kombinationen a^b mit 2 <= a <= 5 und 2 <= b <= 5:

2^2=4, 2^3=8, 2^4=16, 2^5=32
3^2=9, 3^3=27, 3^4=81, 3^5=243
4^2=16, 4^3=64, 4^4=256, 4^5=1024
5^2=25, 5^3=125, 5^4=625, 5^5=3125
Wenn wir diese numerisch ordnen und alle Wiederholungen entfernen, erhalten wir die folgenden 15 verschiedenen Terme:

4, 8, 9, 16, 25, 27, 32, 64, 81, 125, 243, 256, 625, 1024, 3125

Wie viele verschiedene Terme sind in der Folge ab mit 2 <= a <= 100 und 2 <= b <= 100?

"""

kombinationen = []

for a in xrange(1, 101):
    for b in xrange(1, 101):
        if 2 <= a <= 100 and 2 <= b <= 100:
            kombinationen.append(a ** b)

# Set loescht doppelte Eintraege
print len(set(kombinationen))

# print len(set([a ** b for a in range(2, 101) for b in range(2, 101)]))
